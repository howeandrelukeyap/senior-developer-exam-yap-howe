using UnityEngine;

public enum Faction { Citizen, Hacker }
public class Unit : MonoBehaviour
{
    public AnimationHandler AnimationHandler { get; private set; }
    public Movement Movement { get; private set; }
    public Health Health { get; private set; }
    public bool IsMoving
    {
        get
        {
            return Movement.MoveInput.x != 0 || Movement.MoveInput.y != 0;
        }
    }
    public Vector2 MoveInput
    {
        get
        {
            return Movement.MoveInput;
        }
    }
    public bool IsAlive { get; set; }
    public ChipFunctions ChipFunctions { get; private set; }

    public Faction MyFaction;

    protected virtual void Awake()
    {
        Movement = GetComponent<Movement>();
        AnimationHandler = GetComponent<AnimationHandler>();
        Health = GetComponent<Health>();
        ChipFunctions = GetComponent<ChipFunctions>();
    }
}
