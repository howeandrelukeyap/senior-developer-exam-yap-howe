using Photon.Pun;
using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    public event Action <float>OnHealthChanged;
    public event Action OnDeath;

    public float MyMaxValue { get; set; }

    public float MyCurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value > MyMaxValue)
            {
                currentValue = MyMaxValue;
            }
            else if (value < 0)
            {
                currentValue = 0;
            }
            else
            {
                currentValue = value;
            }
        }
    }

    private float currentValue;

    [PunRPC]
    public void SetHealth(float maxHealth)
    {
        MyMaxValue = maxHealth;
        MyCurrentValue = maxHealth;
        OnHealthChanged?.Invoke(MyCurrentValue);
    }

    [PunRPC]
    public void TakeDamage(int dmg)
    {
        MyCurrentValue -= dmg;
        OnHealthChanged?.Invoke(MyCurrentValue);

        if (MyCurrentValue <= 0)
        {
            OnDeath?.Invoke();
            Destroy(gameObject);
        }
    }

    

}
