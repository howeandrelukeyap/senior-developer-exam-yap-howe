using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HackUI : MonoBehaviour
{
    public Button FreezeButton;
    public Button CorruptButton;
    public TextMeshProUGUI NameText;
}
