using Photon.Pun.Demo.PunBasics;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Dropdown : MonoBehaviour
{
    public event Action <PhotonView>OnChangeTarget;

    public Dictionary<string, PhotonView> Units = new Dictionary<string, PhotonView>();

    public TextMeshProUGUI output;

    private TMP_Dropdown dropdown;

    private void Awake()
    {
        dropdown = GetComponent<TMP_Dropdown>();
    }

    private void Start()
    {
        dropdown.onValueChanged.AddListener(delegate {
            ChangeTarget(dropdown);
        });
    }
    public void InitializeDropdown()
    {
        GameManager gameManager = SingletonManager.Get<GameManager>();

        foreach (KeyValuePair<int, GameObject> player in gameManager.Players)
        {
            PhotonView pv = player.Value.GetPhotonView();

            if (pv.IsMine) continue;

            Units.Add(pv.Owner.NickName, pv);
            var option = new TMP_Dropdown.OptionData(pv.Owner.NickName);
            dropdown.options.Add(option);
        }
    }

    private void ChangeTarget(TMP_Dropdown val)
    {
        if (val.options[val.value].text == "None")
        {
            OnChangeTarget?.Invoke(null);
        }
        else
        {
            OnChangeTarget?.Invoke(Units[val.options[val.value].text]);
        }
    }
}
