using System.Collections;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Cinemachine;
using System.Collections.Generic;
using TMPro;

public class GameManager : MonoBehaviourPunCallbacks
{
    public int HackerActorNumber { get; private set; }

    public Dictionary<int, GameObject> Players = new Dictionary<int, GameObject>();

    public PlayerSpawnManager SpawnManager;

    public CinemachineVirtualCamera VirtualCamera;

    [SerializeField]
    private GameObject[] playerPrefabs;

    public TextMeshProUGUI test;
    private void Awake()
    {
        SingletonManager.Register(this);
    }

    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {

            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            Vector3 instantiatePosition = SpawnManager.GetPlayerSpawnPoint();
            GameObject playerObject = PhotonNetwork.Instantiate(playerPrefabs[0].name, instantiatePosition, Quaternion.identity);
            PhotonView pv = playerObject.GetComponent<PhotonView>();
        }

        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;
    }

    public void SetHackerNumber(int num)
    {
        HackerActorNumber = num;
        Debug.Log("HACKER NUM: " + HackerActorNumber);
        test.text = HackerActorNumber.ToString();
    }

    [PunRPC]
    public void GameEnd()
    {
        StartCoroutine(GameEndRoutine());
    }

    public IEnumerator GameEndRoutine()
    {
        yield return new WaitForSeconds(5.0f);

        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }
}
