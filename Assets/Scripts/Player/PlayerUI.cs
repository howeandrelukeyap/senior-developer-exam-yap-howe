using Photon.Pun;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [Header("Hacker Panel")]
    public GameObject HackerPanel;
    public GameObject OpenHackButton;
    public GameObject HackerTerminal;

    [SerializeField]
    private GameObject hackTargetPrefab;

    [Header("Citizen Panel")]
    public GameObject CitizenPanel;
    public Dropdown Dropdown;


    [Header("General References")]
    [SerializeField]
    public TextMeshProUGUI healthText;

    private GameManager gameManager;

    public void HealthChanged(float health)
    {
        healthText.text = health.ToString();
    }

    public void InstantiateHackVictims()
    {
        gameManager = SingletonManager.Get<GameManager>();

        foreach (KeyValuePair<int, GameObject> player in gameManager.Players)
        {
            PhotonView pv = player.Value.GetPhotonView();

            if (pv.IsMine) continue;

            ChipFunctions chip = player.Value.GetComponent<ChipFunctions>();
            HackUI hackUI = Instantiate(hackTargetPrefab, HackerTerminal.transform).GetComponent<HackUI>();
            hackUI.FreezeButton.onClick.AddListener(chip.Freeze);
            hackUI.CorruptButton.onClick.AddListener(chip.Corrupt);
            hackUI.NameText.text = pv.Owner.NickName;
        }
    }

    public void OpenCloseHackPanel()
    {
        OpenHackButton.SetActive(OpenHackButton.activeSelf ? false : true);
        HackerTerminal.SetActive(HackerTerminal.activeSelf ? false : true);
    }
}
