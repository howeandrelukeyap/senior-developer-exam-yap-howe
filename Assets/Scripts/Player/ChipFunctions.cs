using Photon.Pun;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChipFunctions : MonoBehaviourPunCallbacks
{
    private PhotonView target;

    GameManager gameManager;
    public void Freeze()
    {
        photonView.RPC("TakeDamage", RpcTarget.AllBuffered, 1);

    }

    public void Corrupt()
    {
        photonView.RPC("TakeDamage", RpcTarget.AllBuffered, 2);
    }
}
