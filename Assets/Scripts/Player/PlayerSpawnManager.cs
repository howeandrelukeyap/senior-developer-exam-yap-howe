using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnManager : MonoBehaviour
{
    [SerializeField]
    private Transform spawnPoint;

    public Vector3 GetPlayerSpawnPoint()
    {
        return spawnPoint.position;
    }
}
