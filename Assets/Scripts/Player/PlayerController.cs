using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    private Unit unit;

    private void Awake()
    {
        unit = GetComponent<Unit>();
    }

    private void FixedUpdate()
    {
        HandleJoystick();
    }

    public void HandleJoystick()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        unit.Movement.MoveInput = new Vector2(horizontalAxis, verticalAxis);
    }
}
