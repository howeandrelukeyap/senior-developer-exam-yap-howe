using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using UnityEngine.XR;
using Photon.Pun.Demo.PunBasics;
using Photon.Realtime;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject playerUiPrefab;

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private TextMeshProUGUI playerNameText;

    private PlayerController playerController;

    private Unit unit;

    private GameManager gameManager;

    private PlayerUI playerUi;
    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        unit = GetComponent<Unit>();
    }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = SingletonManager.Get<GameManager>();
        photonView.RPC("AddMyself", RpcTarget.AllBuffered);
        playerNameText.text = photonView.Owner.NickName;

        if (PhotonNetwork.IsMasterClient && photonView.IsMine)
        {
            Debug.Log("1 to " + PhotonNetwork.CurrentRoom.PlayerCount + 1);
            int rand = Random.Range(1, PhotonNetwork.CurrentRoom.PlayerCount+1);
            photonView.RPC("SetHackerNumber", RpcTarget.AllBuffered, rand);
        }

        if (photonView.IsMine)
        {
            playerController.enabled = true;
            playerCamera.enabled = true;
            Instantiate(playerCamera);
            gameManager.VirtualCamera.Follow = transform;
            playerUi = Instantiate(playerUiPrefab).GetComponent<PlayerUI>();
            StartCoroutine(DelayedSetup());
        }
        else
        {
            playerController.enabled = false;
            playerCamera.enabled = false;
        }
    }

    private IEnumerator DelayedSetup()
    {
        yield return new WaitForSeconds(1f);

        float newHealth;
        Debug.Log("ACTOR NUM: " + gameManager.HackerActorNumber + " OWNER NUM: " + photonView.OwnerActorNr);
        if (gameManager.HackerActorNumber == photonView.OwnerActorNr)
        {
            newHealth = PhotonNetwork.CurrentRoom.PlayerCount;
            playerUi.HackerPanel.SetActive(true);
            playerUi.InstantiateHackVictims();
            photonView.RPC("SetFaction", RpcTarget.AllBuffered, Faction.Hacker);
        }
        else
        {
            newHealth = 4;
            playerUi.CitizenPanel.SetActive(true);
            playerUi.Dropdown.InitializeDropdown();
            photonView.RPC("SetFaction", RpcTarget.AllBuffered, Faction.Citizen);
        }

        unit.Health.OnHealthChanged += playerUi.HealthChanged;
        photonView.RPC("SetHealth", RpcTarget.AllBuffered, newHealth);
    }

    [PunRPC]
    public void AddMyself()
    {
        if (gameManager == null)
        {
            gameManager = SingletonManager.Get<GameManager>();
        }

        if (!gameManager.Players.ContainsKey(photonView.ViewID))
        {
            gameManager.Players.Add(photonView.ViewID, gameObject);
        }
    }

    [PunRPC]
    public void SetHackerNumber(int number)
    {
        gameManager.SetHackerNumber(number);
    }

    [PunRPC]
    public void SetFaction(Faction faction)
    {
        unit.MyFaction = faction;
    }
}
